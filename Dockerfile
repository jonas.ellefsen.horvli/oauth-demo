FROM node:lts-alpine AS builder

ENV PORT 3000

WORKDIR /src
COPY package.json .
RUN npm install
COPY . .

RUN npm run build
# /src/dist
FROM node:lts-alpine AS production

EXPOSE 3000/tcp

WORKDIR /app
COPY --from=builder /src/package.json .
RUN npm install express path compression dotenv
COPY server.js .
COPY --from=builder /src/build ./build

CMD ["node", "server.js"]
