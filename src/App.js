import React from 'react';
import logo from './logo.svg';
import './App.css';
import {withRouter} from 'react-router-dom';

class App extends React.Component {
    state = {token: '', userInfo: null};

    componentDidMount() {
        this.asyncload();
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.state.token && !this.state.userInfo) {
            fetch('https://gitlab.com/api/v4/user', {
                headers: {
                    'method': 'GET',
                    'Authorization': `Bearer ${this.state.token}`,
                }
            }).then(resp => resp.json().then(data => this.setState({userInfo: data})));
        }
    }

    async asyncload() {
        let location = this.props.location;
        if (location.hash) {
            return this.setState({token: [...new URLSearchParams(location.hash.substring(1)).entries()][0][1]});
        }
        // cheating with since heroku config vars don't seem to get passed to process.env
        let params = new URLSearchParams({
            client_id: (process.env.REACT_APP_CLIENT_ID || 'e52bd546a0367d2b9af9a453589ca4d289468216e29cbae24be837995a248cec'),
            redirect_uri: (process.env.REACT_APP_REDIRECT_URI || 'https://jonas-oauth-demo.herokuapp.com'),
            response_type: 'token'
        });
        window.location = `https://gitlab.com/oauth/authorize?${params.toString()}`;
    }

    render() {
        return (
            <div className="App">
                <header className="App-header">
                    <div>{this.state.userInfo ? UserCard(this.state.userInfo) : <div>Loading</div>}</div>
                </header>
            </div>
        );
    }
}

function UserCard(userInfo) {
    return (
        <div>
            <img src={userInfo.avatar_url} alt="logo"/>
            <div className='userProperty'>
                <div className='userPropertyTitle'>{'Name:'}</div>
                <div className='userPropertyValue'>{userInfo.name}</div>
            </div>
            <div className='userProperty'>
                <div className='userPropertyTitle'>{'Username:'}</div>
                <div className='userPropertyValue'>{userInfo.name}</div>
            </div>
            <div className='userProperty'>
                <div className='userPropertyTitle'>{'Email:'}</div>
                <div className='userPropertyValue'>{userInfo.email}</div>
            </div>
        </div>
    );
}

export default withRouter(App);
